/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.EggRecipe;

/**
 *
 * @author lendle
 */
public class EggRecipeItem {
    private String recipeName=null;
    private int numEggs=-1;
    private String description=null;

    public EggRecipeItem(String recipeName, int numEggs, String description) {
        this.recipeName=recipeName;
        this.numEggs=numEggs;
        this.description=description;
    }
    
    public String getRecipeName() {
        return recipeName;
    }

    public void setRecipeName(String recipeName) {
        this.recipeName = recipeName;
    }

    public int getNumEggs() {
        return numEggs;
    }

    public void setNumEggs(int numEggs) {
        this.numEggs = numEggs;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    
}
