package com.example.EggRecipe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EggRecipeApplication {

	public static void main(String[] args) {
		SpringApplication.run(EggRecipeApplication.class, args);
	}

}
