/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.EggRecipe;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

/**
 *
 * @author lendle
 */
@Controller
public class EggRecipeController {
    @GetMapping("/")
    public String indexAction(Model model){
        return "index";
    }
    
    public String addRecipeAction(){
        return "addRecipe";
    }
    
    public String addRecipeAction(EggRecipeItem item){
        return "redirect:/";
    }
    
    public String deleteRecipeAction(String name){
        int index=-1;
        for(int i=0; i<EggRecipeDB.getItems().size(); i++){
            EggRecipeItem item=EggRecipeDB.getItems().get(i);
            if(item.getRecipeName().equals(name)){
                index=i;
                break;
            }
        }
        if(index!=-1){
            EggRecipeDB.getItems().remove(index);
        }
        return "redirect:/";
    }
}
