/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.EggRecipe;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author lendle
 */
public class EggRecipeDB {
    private static List<EggRecipeItem> items=new ArrayList<>();
    static{
        items.add(new EggRecipeItem("Fried Egg", 1, "Crack the eggs into the pan. As the oil gets hotter you’ll see it start to change the color of the eggs. If the oil starts to spit it’s because it’s too hot, so turn the heat right down. Cook until the tops of the whites are set but the yolk is still runny."));
        items.add(new EggRecipeItem("蛋炒飯", 2, "使用隔夜飯的原因是，白飯經過冷藏後，水分會變的比較少，也就是會變的稍微乾一些，炒飯需要用比較沒有水分的白飯，這樣會比較好炒"));
    }
    public static List<EggRecipeItem> getItems(){
        return items;
    }
}
